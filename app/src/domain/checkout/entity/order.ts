import OrderItem from "./order_item";
export default class Order {
  private _id: string;
  private _customerId: string;
  private _items: OrderItem[];
  private _total: number;

  constructor(id: string, customerId: string, items: OrderItem[]) {
    this._id = id;
    this._customerId = customerId;
    this._items = items;
    this._total = this.total();
    this.validate();
  }

  get id(): string {
    return this._id;
  }

  get customerId(): string {
    return this._customerId;
  }

  get items(): OrderItem[] {
    return this._items;
  }

  validate(): boolean {
    if (this._id.length === 0) {
      throw new Error("Id e obrigatório");
    }
    if (this._customerId.length === 0) {
      throw new Error("Cliente é obrigatório");
    }
    if (this._items.length === 0) {
      throw new Error("Itens São obrigatórios");
    }

    if (this._items.some((item) => item.quantity <= 0)) {
      throw new Error("Quantidade deve ser maior que 0");
    }

    return true;
  }

  total(): number {
    return this._items.reduce((acc, item) => acc + item.price, 0);
  }

  addItem(item: OrderItem): void {
    this._items.push(item);
    this._total = this.total();
  }
}
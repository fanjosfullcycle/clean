import Customer from "../../customer/entity/customer";
import Order from "../entity/order";
import OrderItem from "../entity/order_item";
import OrderService from "./order.service";

describe("order service unit tests", () => {

    it("Criar nova ordem", () => {
        const customer = new Customer("c1", "Customer 1");
        const item1 = new OrderItem("i1", "Item 1", 10, "p1", 1);

        const order = OrderService.placeOrder(customer, [item1]);

        expect(customer.rewardPoints).toBe(5);
        expect(order.total()).toBe(10);
    });

    it("Somar total de pedidos", () => {
        const item1 = new OrderItem("i1", "item 1", 10, "p1", 1);
        const item2 = new OrderItem("i2", "item 2", 20, "p2", 1);
        const item3 = new OrderItem("i3", "item 3", 30, "p3", 1);
        
        const pedido1 = new Order("o1", "c1", [item1]);
        const pedido2 = new Order("o2", "c2", [item2, item3]);

        const total = OrderService.total([pedido1, pedido2]);

        expect(total).toBe(60);
    });
});
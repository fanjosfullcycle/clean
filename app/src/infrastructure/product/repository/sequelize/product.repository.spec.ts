import { Sequelize } from "sequelize-typescript";
import Product from "../../../../domain/product/entity/product";
import ProductModel from "./product.model";


import ProductRepository from "./product.reporitory";

describe("Product repository test", () => {
    
    let sequelize: Sequelize;

    beforeEach(async () => {
        sequelize = new Sequelize({
            dialect: "sqlite",
            storage: ":memory:",
            logging: false,
            sync: { force: true },
        });

        sequelize.addModels([ProductModel]);
        await sequelize.sync();
    });

    afterEach(async () => {
        await sequelize.close();
    });

    it("criar produto", async() => {
        const productRepository = new ProductRepository();
        const product = new Product("1", "Produto 1", 100);

        await productRepository.create(product);

        const productModel = await ProductModel.findOne({where: {id: "1"}});

        expect(productModel.toJSON()).toStrictEqual({
            id: "1",
            name: "Produto 1",
            price: 100
        });
    });

    it("atualizar um produto", async () => {
        const productRepository = new ProductRepository();
        const product = new Product("1", "Produto 1", 100);

        await productRepository.create(product);

        product.changeName("Produto 2");
        product.changePrice(200);

        await productRepository.update(product);

        const productModel2 = await ProductModel.findOne({ where: {id: "1" }});

        expect(productModel2.toJSON()).toStrictEqual({
            id: "1",
            name: "Produto 2",
            price: 200
        });
    });

    it("buscar um produto", async () => {
        const productRepository = new ProductRepository();
        const product = new Product("1", "Produto 1", 100);
        await productRepository.create(product);
        const product2 = new Product("2", "Produto 2", 100);
        await productRepository.create(product2);

        const foundProducts = await productRepository.findAll();
        const products = [product, product2];

        expect(products).toEqual(foundProducts);
    });

    it("busca todos os produts", async () => {
        const productRepository = new ProductRepository();
        const product = new Product("1", "Produto 1", 100);

        await productRepository.create(product);
    });
});
import Order from "./order";
import OrderItem from "./order_item";

describe("Order unit tests", () => {
    
    it("Gera erro quando id está vazio", () => {
        expect(() => {
            let order = new Order("", "123", []);
        }).toThrowError("Id e obrigatório");
    });

    it("Gera erro quando cliente está vazio", () => {
        expect(() => {
            let order = new Order("1", "", []);
        }).toThrowError("Cliente é obrigatório");
    });

    it("Gera erro quando itens estão vazios", () => {
        expect(() => {
            let order = new Order("1", "123", []);
        }).toThrowError("Itens São obrigatórios");
    });

    it("Gera erro quando quantidade é igual a 0", () => {
        const item = new OrderItem("1", "item 1", 50, "1", 0);

        expect(() => {
            let order = new Order("1", "123", [item]);
        }).toThrowError("Quantidade deve ser maior que 0");
    });

    it("Testa total", () => {
        
        const item = new OrderItem("1", "item 1", 50, "1", 1);
        const item2 = new OrderItem("1", "item 2", 100, "1", 2);
        const order = new Order("o1", "c1", [item, item2]);

        const total = order.total();

        expect(total).toBe(250);

    });
});
import Address from "../value-object/address";
import Customer from "./customer";

describe("Customer unit tests", () => {
    it("Gera erro quando id está vazio", () => {
        expect(() => {
            let customer = new Customer("", "John");
        }).toThrowError("Id é obrigatório");
    });

    it("Gera erro quando nome está vazio", () => {
        expect(() => {
            let customer = new Customer("1", "");
        }).toThrowError("Nome é obrigatório");
    });

    it("Mudar o nome", () => {
        //Arrange
        const customer = new Customer("123", "John");

        //Act
        customer.changeName("Jane");

        //Assert
        expect(customer.name).toBe("Jane");
    });

    it("Ativar cliente", () => {
        //Arrange
        const customer = new Customer("1", "Customer 1");
        const address = new Address("Street 1", 123, "07040-444", "Sao Paulo");
        customer.Address = address;

        //Act
        customer.activate();

        //Assert
        expect(customer.isActive()).toBe(true);
    });

    it("Erro ao ativar cliente sem endereço", () => {
        
        expect(() => {
            const customer = new Customer("1", "Customer 1");
            customer.activate();
        }).toThrowError("É necessario que o cliente tenha endereço ao ser ativado");
    });

    it("Ativar cliente", () => {
        //Arrange
        const customer = new Customer("1", "Customer 1");
    
        //Act
        customer.deactivate();

        //Assert
        expect(customer.isActive()).toBe(false);
    });

    it("acrescentar reward points", () => {
        const customer = new Customer("1", "Customer 1");
        expect(customer.rewardPoints).toBe(0);

        customer.addRewardPoints(10);
        expect(customer.rewardPoints).toBe(10);

        customer.addRewardPoints(10);
        expect(customer.rewardPoints).toBe(20);
    });
});
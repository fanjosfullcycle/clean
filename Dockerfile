FROM nginx:latest

WORKDIR '/usr/share/nginx/html'
RUN apt-get update
RUN apt install nodejs -y
RUN apt install npm -y
RUN npm install -g typescript -y
RUN npm install -g n
RUN n stable

RUN npm i -D @types/jest ts-node --save-dev
RUN npm i -D @swc/jest @swc/cli @swc/core

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]